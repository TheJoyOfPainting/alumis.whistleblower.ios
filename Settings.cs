﻿using System;
namespace Alumis.Whistleblower.Authenticator.iOS
{
    public static class Settings
    {
        public const string PRIVATE_KEY_APPLICATION_TAG = "whistleblower-private-key";
        public const string PUBLIC_KEY_APPLICATION_TAG = "whistleblower-public-key";
    }
}
