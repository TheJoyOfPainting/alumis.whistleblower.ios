﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using Firebase.CloudMessaging;
using Firebase.Core;
using Foundation;
using Security;
using UIKit;
using UserNotifications;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate, IUNUserNotificationCenterDelegate
    {
        // class-level declarations

        public override UIWindow Window
        {
            get;
            set;
        }

        public FirebaseMessagingService FirebaseMessagingService = new FirebaseMessagingService();
        public byte[] ServerChallenge;
        public string ServerChallengeBaseUrl;
        bool _isProcessingChallenge;
        readonly object _synchronizationObject = new object();

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            // Remote Notifications
            UNUserNotificationCenter.Current.Delegate = this;

            var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Sound;
            UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (granted, error) => {
                Console.WriteLine(granted);
            });
            UIApplication.SharedApplication.RegisterForRemoteNotifications();

            // Firebase service
            FirebaseMessagingService.Configure();
            FirebaseMessagingService.RemoteMessagingEvent += FirebaseMessageEventHandler;

            // Launch options
            if(launchOptions != null && launchOptions.TryGetValue(UIApplication.LaunchOptionsRemoteNotificationKey, out NSObject appData))
            {
                if( ((NSDictionary)appData).TryGetValue(new NSString("Challenge"), out NSObject challengeObject))
                {
                    ServerChallenge = Convert.FromBase64String(challengeObject.ToString());   
                }

                if (((NSDictionary)appData).TryGetValue(new NSString("BaseUrl"), out NSObject baseUrlObject))
                {
                    ServerChallengeBaseUrl = baseUrlObject.ToString();
                }
            }

            return true;
        }

        public override void OnResignActivation(UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground(UIApplication application)
        {
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
        }

        public override void WillEnterForeground(UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
            Console.WriteLine("Will enter foreground");
        }

        public override void OnActivated(UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
            Console.WriteLine("On activated");

            InvokeOnMainThread(async () =>
            {
                if (ServerChallenge != null && ServerChallengeBaseUrl != null)
                {
                    await PresentUserIdentityViewControllerWithChallenge(ServerChallenge, ServerChallengeBaseUrl);
                }
                else
                {
                    var mainViewController = ((MainViewController)Window.RootViewController);

                    if(mainViewController.PresentedViewController == null)
                    {
                        await mainViewController.PresentQRCodeReaderViewControllerAsync();
                    }
                }
            }); 
        }

        public override void WillTerminate(UIApplication application)
        {
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            // If you are receiving a notification message while your app is in the background,
            // this callback will not be fired till the user taps on the notification launching the application.
            // TODO: Handle data of notification

            // With swizzling disabled you must let Messaging know about the message, for Analytics
            //Messaging.SharedInstance.AppDidReceiveMessage (userInfo);

            // Print full message.
            Console.WriteLine("ReceivedRemoteNotification");
            Console.WriteLine(userInfo);

            userInfo.TryGetValue(new NSString("Challenge"), out NSObject challengeObject);
            var newChallenge = Convert.FromBase64String(challengeObject.ToString());

            userInfo.TryGetValue(new NSString("BaseUrl"), out NSObject baseUrlObject);

            if (ServerChallenge == null || !newChallenge.SequenceEqual(ServerChallenge))
                ServerChallenge = newChallenge;
        } 

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            // If you are receiving a notification message while your app is in the background,
            // this callback will not be fired till the user taps on the notification launching the application.
            // TODO: Handle data of notification

            // With swizzling disabled you must let Messaging know about the message, for Analytics
            //Messaging.SharedInstance.AppDidReceiveMessage (userInfo);

            // Print full message.
            Console.WriteLine("DidReceiveRemoteNotification");
            Console.WriteLine(userInfo);

            userInfo.TryGetValue(new NSString("Challenge"), out NSObject challengeObject);
            var newChallenge = Convert.FromBase64String(challengeObject.ToString());

            userInfo.TryGetValue(new NSString("BaseUrl"), out NSObject baseUrlObject);

            if (ServerChallenge == null || !newChallenge.SequenceEqual(ServerChallenge))
            {
                ServerChallenge = newChallenge;

                if(UIApplication.SharedApplication.ApplicationState == UIApplicationState.Active)
                {
                    PresentUserIdentityViewControllerWithChallenge(ServerChallenge, baseUrlObject.ToString());
                }
            }

            completionHandler(UIBackgroundFetchResult.NewData);
        }

        public void FirebaseMessageEventHandler(NSDictionary appData)
        {
            if (UIApplication.SharedApplication.ApplicationState != UIApplicationState.Active)
                return;

            InvokeOnMainThread(async () =>
            {
                appData.TryGetValue(new NSString("Challenge"), out NSObject challengeObject);
                ServerChallenge = Convert.FromBase64String(challengeObject.ToString());

                appData.TryGetValue(new NSString("BaseUrl"), out NSObject baseUrlObject);
                var baseUrl = baseUrlObject.ToString();
                ServerChallengeBaseUrl = baseUrl;

                await PresentUserIdentityViewControllerWithChallenge(ServerChallenge, baseUrl);
            });
        }

        public async Task PresentUserIdentityViewControllerWithChallenge(byte[] challenge, string baseUrl)
        {
            lock(_synchronizationObject)
            {
                if (_isProcessingChallenge)
                    return;

                else
                    _isProcessingChallenge = true;
            }

            var mainViewController = ((MainViewController)Window.RootViewController);

            await mainViewController.PresentUserIdentityViewControllerAsync();

            var success = await mainViewController.UserIdentityViewController.ProcessServerChallengeAsync(challenge, baseUrl);

            if (success)
                ServerChallenge = null;

            _isProcessingChallenge = false;
        }
    }
}

