using Firebase.CloudMessaging;
using Foundation;
using LocalAuthentication;
using Newtonsoft.Json;
using Security;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UIKit;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public partial class ETFAViewController : UIViewController
    {
        public ETFAViewController(IntPtr handle) : base(handle)
        {
            _disposables.Add(_submitEnabled = new ComputedObservable<bool>(DetermineShould));

            _disposables.Add(_submitEnabled.Subscribe((bool oldValue, bool enabled) =>
            {
                if(enabled)
                {
                    SubmitBtn.Enabled = true;
                    SubmitBtn.Alpha = 1f;
                }
                else
                {
                    SubmitBtn.Enabled = false;
                    SubmitBtn.Alpha = 0.6f;
                }
            }));
        }

        ComputedObservable<bool> _submitEnabled;
        Observable<string> _usernameValue = new Observable<string>("");
        Observable<string> _passwordValue = new Observable<string>("");

        List<IDisposable> _disposables = new List<IDisposable>();

        bool DetermineShould(ComputedObservable<bool> o)
        {
            var fcmToken = ((AppDelegate)UIApplication.SharedApplication.Delegate).FirebaseMessagingService.FcmToken;

            if (o.GetValue(_usernameValue).Length > 0 && o.GetValue(_passwordValue).Length > 0 && o.GetValue(fcmToken) != null)
                return true;

            return false;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            ApplyConstraints();

            AttachEventListeners();

            SubmitBtn.Enabled = false;
            SubmitBtn.Alpha = 0.6f;

            ActivityIndicator.Hidden = true;
        }

        public async Task SubmitAsync()
        {
            /*
            SubmitBtn.Hidden = true;
            ActivityIndicator.Hidden = false;
            ActivityIndicator.StartAnimating();

            SecKey privateKey, publicKey;

            privateKey = Utils.QueryKey(SecKeyType.EC, Settings.PRIVATE_KEY_APPLICATION_TAG);
            publicKey = Utils.QueryKey(SecKeyType.EC, Settings.PUBLIC_KEY_APPLICATION_TAG);

            if(privateKey == null || publicKey == null)
            {
                try
                {
                    privateKey = Utils.CreateKeyPairInSecureEnclave(SecKeyType.EC, 256, Settings.PRIVATE_KEY_APPLICATION_TAG, Settings.PUBLIC_KEY_APPLICATION_TAG);
                }
                catch(CryptographicException)
                {
                    PresentAlert("Feil", "Det oppsto en feil ved generering av kryptografisk nøkkelpar");   
                }
                publicKey = Utils.QueryKey(SecKeyType.EC, Settings.PUBLIC_KEY_APPLICATION_TAG);   
            }

            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(new[] {
                    new KeyValuePair<string, string>("username", _usernameValue.Value),
                    new KeyValuePair<string, string>("password", _passwordValue.Value),
                    new KeyValuePair<string, string>("deviceId", UIDevice.CurrentDevice.IdentifierForVendor.ToString()),
                    new KeyValuePair<string, string>("deviceName", UIDevice.CurrentDevice.Name),
                    new KeyValuePair<string, string>("deviceType", ((int)DeviceType.iOS).ToString(CultureInfo.InvariantCulture) ),
                    new KeyValuePair<string, string>("devicePushId", Messaging.SharedInstance.FcmToken),
                    new KeyValuePair<string, string>("devicePublicKey", Convert.ToBase64String(publicKey.ConvertToDER())),
                    new KeyValuePair<string, string>("devicePublicKeyFormat", "X.509"),
                    new KeyValuePair<string, string>("devicePublicKeyAlgorithm", "EC")});

                HttpResponseMessage result = null;

                try
                {
                    result = await client.PostAsync($"{Settings.SERVER_URL}Admin/EnableTwoFactorAuthentication", content);
                    result.EnsureSuccessStatusCode();

                    PresentAlert("Vellyket", "Denne enheten er nå registrert på din bruker. Du kan nå lukke denne appen.");
                }
                catch(HttpRequestException)
                {
                    if(result.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        PresentAlert("Feil", "Ugyldig brukernavn eller passord");

                        Utils.RemoveKeyPair(SecKeyType.EC, Settings.PRIVATE_KEY_APPLICATION_TAG, Settings.PUBLIC_KEY_APPLICATION_TAG);
                    }
                    else if(result.StatusCode == HttpStatusCode.Forbidden)
                    {
                        PresentAlert("Feil", "Det fins allerede en enhet knyttet til din bruker");

                        Utils.RemoveKeyPair(SecKeyType.EC, Settings.PRIVATE_KEY_APPLICATION_TAG, Settings.PUBLIC_KEY_APPLICATION_TAG);
                    }
                }
                finally
                {
                    ActivityIndicator.StopAnimating();
                    ActivityIndicator.Hidden = true;
                    SubmitBtn.Hidden = false;
                }
            } */
        }

        public void PresentAlert(string title, string message, IEnumerable<UIAlertAction> actions = null)
        {
            var alertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);

            if(actions == null)
            {
                alertController.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Default, null));
            }
            else
            {
                foreach(var a in actions)
                {
                    alertController.AddAction(a);
                }
            }

            PresentViewController(alertController, true, null);
        }

        private void ApplyConstraints()
        {
            if (View.Bounds.Width < 375.1f)
            {
                nfloat widthFactor = 0.8f;

                Image.WidthAnchor.ConstraintEqualTo(View.WidthAnchor, widthFactor).Active = true;
                Text.WidthAnchor.ConstraintEqualTo(View.WidthAnchor, widthFactor).Active = true;
                UsernameInput.WidthAnchor.ConstraintEqualTo(View.WidthAnchor, widthFactor).Active = true;
                PasswordInput.WidthAnchor.ConstraintEqualTo(View.WidthAnchor, widthFactor).Active = true;
                SubmitBtn.WidthAnchor.ConstraintEqualTo(View.WidthAnchor, 0.29f).Active = true;
                SubmitBtn.Layer.CornerRadius = View.Bounds.Width * 0.29f / 4;
            }
            else
            {
                Image.WidthAnchor.ConstraintEqualTo(350f).Active = true;
                Text.WidthAnchor.ConstraintEqualTo(350f).Active = true;
                UsernameInput.WidthAnchor.ConstraintEqualTo(350f).Active = true;
                PasswordInput.WidthAnchor.ConstraintEqualTo(350f).Active = true;
                SubmitBtn.WidthAnchor.ConstraintEqualTo(122f).Active = true;
                SubmitBtn.Layer.CornerRadius = 122f / 4;
            }   
        }

        private void AttachEventListeners()
        {
            UsernameInput.Ended += delegate {

                _usernameValue.Set(UsernameInput.Text);
            };

            PasswordInput.Ended += delegate {

                _passwordValue.Set(PasswordInput.Text);
            };

            SubmitBtn.TouchUpInside += async (sender, e) =>
            {
               await SubmitAsync();    
            };

            View.AddGestureRecognizer(new UITapGestureRecognizer(() =>
            {
                UIApplication.SharedApplication.KeyWindow.EndEditing(true);
            }));
        }
    }
}