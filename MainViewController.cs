using Foundation;
using System;
using System.Threading.Tasks;
using UIKit;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public partial class MainViewController : UIViewController
    {
        public MainViewController (IntPtr handle) : base (handle)
        {
        }

        public QRCodeReaderViewController QRCodeReaderViewController { get; private set; }
        public UserIdentityViewController UserIdentityViewController { get; private set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var mainStoryboard = UIStoryboard.FromName("Main", null);

            QRCodeReaderViewController = (QRCodeReaderViewController)mainStoryboard.InstantiateViewController("QRCodeReaderViewController");
            UserIdentityViewController = (UserIdentityViewController)mainStoryboard.InstantiateViewController("UserIdentityViewController");
        }

        public async Task PresentQRCodeReaderViewControllerAsync()
        {
            var vc = GetMostPresentViewController();

            var qrCodeReaderType = typeof(QRCodeReaderViewController);

            for (;;)
            {
                if (vc == null || qrCodeReaderType.IsAssignableFrom(vc.GetType()))
                    break;

                vc = vc.ParentViewController;

                await DismissViewControllerAsync(true);
            }

            if (vc == null)
                await PresentViewControllerAsync(QRCodeReaderViewController, true);
        }

        public async Task PresentUserIdentityViewControllerAsync()
        {
            var vc = GetMostPresentViewController();

            var etfaType = typeof(UserIdentityViewController);

            for (; ; )
            {
                if (vc == null || etfaType.IsAssignableFrom(vc.GetType()))
                    break;

                vc = vc.ParentViewController;

                await DismissViewControllerAsync(true);
            }

            if (vc == null)
                await PresentViewControllerAsync(UserIdentityViewController, true);
        }

        public UIViewController GetMostPresentViewController()
        {
            var vc = PresentedViewController;

            if (vc == null)
                return null;

            while(vc.PresentedViewController != null)
            {
                vc = vc.PresentedViewController;
            }

            return vc;
        }
    }
}