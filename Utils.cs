﻿using System;
using System.Collections.Generic;
using Foundation;
using Security;
using UIKit;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public static class Utils
    {
        public static UIViewController GetMostPresentViewController()
        {
            var vc = UIApplication.SharedApplication.KeyWindow.RootViewController;

            while(vc.PresentedViewController != null)
            {
                vc = vc.PresentedViewController;
            }

            return vc;
        }

        public static byte[] ConvertToDER(this SecKey publicKey)
        {
            var x9_62HeaderECHeader = new byte[] {
                0x30, 0x59,
                0x30, 0x13,
                0x06, 0x07, 0x2A, 0x86, 0x48, 0xCE, 0x3D, 0x02, 0x01,
                0x06, 0x08, 0x2A, 0x86, 0x48, 0xCE, 0x3D, 0x03, 0x01,
                0x07, 0x03, 0x42, 0x00
            };

            var publicKeyBytes = publicKey.GetExternalRepresentation().ToArray();

            var der = new List<byte>();

            der.AddRange(x9_62HeaderECHeader);
            der.AddRange(publicKey.GetExternalRepresentation().ToArray());

            return der.ToArray();
        }

        public static SecKey CreateKeyPairInSecureEnclave(SecKeyType type, int keySize, string privateKeyApplicationTag, string publicKeyApplicationTag)
        {
            var keyParameters = new SecKeyGenerationParameters
            {
                KeyType = type,
                KeySizeInBits = keySize,
                TokenID = SecTokenID.SecureEnclave,
                PrivateKeyAttrs = new SecKeyParameters
                {
                    IsPermanent = true,
                    ApplicationTag = NSData.FromString(privateKeyApplicationTag),
                    AccessControl = new SecAccessControl(SecAccessible.WhenUnlocked, SecAccessControlCreateFlags.PrivateKeyUsage | SecAccessControlCreateFlags.UserPresence)
                },
                PublicKeyAttrs = new SecKeyParameters
                {
                    IsPermanent = true,
                    ApplicationTag = NSData.FromString(publicKeyApplicationTag),
                    AccessControl = new SecAccessControl(SecAccessible.WhenUnlocked, SecAccessControlCreateFlags.UserPresence)
                }
            };

            var key = SecKey.CreateRandomKey(keyParameters, out NSError error);
            if (key == null)
                throw new NSErrorException(error);

            return key;
        }

        public static SecKey QueryKey(SecKeyType type, string applicationTag)
        {
            using (var query = new SecRecord(SecKind.Key))
            {
                query.ApplicationTag = NSData.FromString(applicationTag);
                query.KeyType = type;

                var key = (SecKey)SecKeyChain.QueryAsConcreteType(query, out SecStatusCode code);

                if (code == SecStatusCode.Success)
                    return key;

                if (code == SecStatusCode.ItemNotFound)
                    return null;

                throw new Exception($"Failed to query keychain for private key. {code.ToString()}");
            }
        }

        public static bool RemoveKeyPair(SecKeyType type, string privateKeyApplicationTag, string publicKeyApplicationTag)
        {
            SecStatusCode removePrivateKeyCode, removePublicKeyCode;

            using(var query = new SecRecord(SecKind.Key))
            {
                query.KeyType = type;
                query.ApplicationTag = privateKeyApplicationTag;

                removePrivateKeyCode = SecKeyChain.Remove(query);
            }

            using (var query = new SecRecord(SecKind.Key))
            {
                query.KeyType = type;
                query.ApplicationTag = publicKeyApplicationTag;

                removePublicKeyCode = SecKeyChain.Remove(query);
            }

            return removePublicKeyCode == SecStatusCode.Success && removePrivateKeyCode == SecStatusCode.Success;
        }
    }
}
