using Firebase.CloudMessaging;
using Foundation;
using Security;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using UIKit;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public partial class UserIdentityViewController : UIViewController
    {
        public UserIdentityViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (View.Bounds.Width < 375.1f)
            {
                Image.WidthAnchor.ConstraintEqualTo(View.WidthAnchor, 0.767f).Active = true;
                Text.WidthAnchor.ConstraintEqualTo(View.WidthAnchor, 0.767f).Active = true;
                Heading.WidthAnchor.ConstraintEqualTo(View.WidthAnchor, 0.767f).Active = true;
            }
            else
            {
                Image.WidthAnchor.ConstraintEqualTo(350f).Active = true;
                Text.WidthAnchor.ConstraintEqualTo(350f).Active = true;
                Heading.WidthAnchor.ConstraintEqualTo(350f).Active = true;
            }
        }

        public void PresentAlert(string title, string message, IEnumerable<UIAlertAction> actions = null)
        {
            var alertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);

            if (actions == null)
            {
                alertController.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Default, null));
            }
            else
            {
                foreach (var a in actions)
                {
                    alertController.AddAction(a);
                }
            }

            PresentViewController(alertController, true, null);
        }

        public async Task<bool> ProcessServerChallengeAsync(byte[] challenge, string baseUrl)
        {
            try
            {
                var privateKey = Utils.QueryKey(SecKeyType.EC, Settings.PRIVATE_KEY_APPLICATION_TAG);
                var signature = privateKey.CreateSignature(SecKeyAlgorithm.EcdsaSignatureMessageX962Sha256, NSData.FromArray(challenge), out NSError error);

                if (signature == null || error != null)
                {
                    throw new CryptographicException();
                }

                using (var client = new HttpClient())
                {
                    var content = new FormUrlEncodedContent(new[] {
                    new KeyValuePair<string, string>("firebaseToken", Messaging.SharedInstance.FcmToken),
                    new KeyValuePair<string, string>("challenge", Convert.ToBase64String(challenge)),
                    new KeyValuePair<string,string>("signature", Convert.ToBase64String(signature.ToArray()) )});

                    var request = await client.PostAsync($"{baseUrl}Api/RespondToChallenge", content);

                    request.EnsureSuccessStatusCode();

                    PresentAlert("Vellyket", "Du er nå logged inn");

                    return true;
                }

                Console.WriteLine("");
            }
            catch(CryptographicException)
            {
                PresentAlert("Feil", "Det oppsto en feil ved signering av melding fra server");
            }
            catch (OperationCanceledException)
            {
                PresentAlert("Tidsavbrudd", "Sjekk din internett forbindelse og prøv på nytt");
            }
            catch (HttpRequestException)
            {
                PresentAlert("Feil", "");
            }

            return false;
        }
    }

}