using AVFoundation;
using CoreFoundation;
using Firebase.CloudMessaging;
using Foundation;
using Security;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using UIKit;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public partial class QRCodeReaderViewController : UIViewController
    {
        public QRCodeReaderViewController (IntPtr handle) : base (handle)
        {
        }

        UIView _cameraView;
        AVCaptureDevice _captureDevice;
        AVCaptureSession _captureSession;
        AVCaptureVideoPreviewLayer _previewLayer;
        public bool HasCameraPermission
        {
            get
            {
                return AVCaptureDevice.GetAuthorizationStatus(AVAuthorizationMediaType.Video) == AVAuthorizationStatus.Authorized;   
            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            _cameraView = new UIView()
            {
                Frame = View.Bounds,
                Hidden = true
            };
            View.AddSubview(_cameraView);

            ScanBtn.TouchUpInside += (object sender, EventArgs e) => 
            {
                StartQRCodeScanning();
            };
        }

        public void StartQRCodeScanning()
        {
            if(HasCameraPermission == false)
            {
                RequestCameraPermission((bool granted) =>
                {
                    if(granted)
                    {
                        StartQRCodeScanning();
                        return;
                    }
                    else
                    {
                        PresentErrorMessage("", "");
                    }
                });
            }

            _captureSession = new AVCaptureSession();
            _captureSession.BeginConfiguration();

            _captureDevice = AVCaptureDevice.GetDefaultDevice(AVCaptureDeviceType.BuiltInDuoCamera, AVMediaType.Video, AVCaptureDevicePosition.Back)
                ?? AVCaptureDevice.GetDefaultDevice(AVCaptureDeviceType.BuiltInWideAngleCamera, AVMediaType.Video, AVCaptureDevicePosition.Back)
                                               ?? AVCaptureDevice.GetDefaultDevice(AVCaptureDeviceType.BuiltInWideAngleCamera, AVMediaType.Video, AVCaptureDevicePosition.Front);

            if(_captureDevice == null)
            {
                PresentErrorMessage("","");
                return;
            }

            var deviceInput = AVCaptureDeviceInput.FromDevice(_captureDevice, out NSError error);

            if(deviceInput == null || error != null)
            {
                PresentErrorMessage("", "");
                return;
            }

            _captureSession.AddInput(deviceInput);

            var metadataOutput = new AVCaptureMetadataOutput();
            var metaDataObjectDelegate = new MetaDataObjectDelegate
            {
                DidOutputMetadataObjectsAction = DidOutputMetadataObjects
            };

            metadataOutput.SetDelegate(metaDataObjectDelegate, DispatchQueue.MainQueue);
            _captureSession.AddOutput(metadataOutput);
            metadataOutput.MetadataObjectTypes = AVMetadataObjectType.QRCode;

            _cameraView.Layer.AddSublayer(_previewLayer = new AVCaptureVideoPreviewLayer(_captureSession)
            {
                VideoGravity = AVLayerVideoGravity.ResizeAspectFill,
                Frame = _cameraView.Bounds
            });
            _cameraView.Hidden = false;

            _captureSession.CommitConfiguration();
            _captureSession.StartRunning();
        }

        public void StopQRCodeScanning()
        {
            if(_captureSession != null)
            {
                _captureSession.StopRunning();
                _captureSession.Dispose();
                _captureSession = null;
            }

            if(_previewLayer != null)
            {
                _previewLayer.RemoveFromSuperLayer();
                _previewLayer.Dispose();
                _previewLayer = null;
            }

            if(_captureDevice != null)
            {
                _captureDevice.Dispose();
                _captureDevice = null;
            }
        }

        public void RequestCameraPermission(Action<bool> completionHandler)
        {
            var status = AVCaptureDevice.GetAuthorizationStatus(AVAuthorizationMediaType.Video);

            if(status == AVAuthorizationStatus.Restricted)
            {
                PresentErrorMessage("Feil", "Du har ikke tilstrekkelige rettigheter til å kunne bruke kameraet på enheten");
            }
            else if(status != AVAuthorizationStatus.Authorized)
            {
                AVCaptureDevice.RequestAccessForMediaType(AVAuthorizationMediaType.Video, (bool granted) =>
                {
                    completionHandler.Invoke(granted);
                });
            }

            completionHandler.Invoke(false);
        }

        public void PresentErrorMessage(string title, string message)
        {
            var alertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);

            alertController.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Default, null));

            PresentViewController(alertController, true, null);
        }

        public void DidOutputMetadataObjects(AVCaptureMetadataOutput captureOutput, AVMetadataObject[] metadataObjects, AVCaptureConnection connection)
        {
            StopQRCodeScanning();

            InvokeOnMainThread(async () =>
            {
                if (metadataObjects.Length == 1)
                {
                    var value = metadataObjects[0].ValueForKey(new NSString("stringValue")).ToString();
                    var i = value.IndexOf(" ", StringComparison.InvariantCulture);

                    var baseUrl = value.Substring(0, i);
                    var hash = value.Substring(i + 1);

                    SecKey privateKey, publicKey;

                    privateKey = Utils.QueryKey(SecKeyType.EC, Settings.PRIVATE_KEY_APPLICATION_TAG);
                    publicKey = Utils.QueryKey(SecKeyType.EC, Settings.PUBLIC_KEY_APPLICATION_TAG);

                    if (privateKey == null || publicKey == null)
                    {
                        try
                        {
                            privateKey = Utils.CreateKeyPairInSecureEnclave(SecKeyType.EC, 256, Settings.PRIVATE_KEY_APPLICATION_TAG, Settings.PUBLIC_KEY_APPLICATION_TAG);
                        }
                        catch (CryptographicException)
                        {
                            PresentAlert("Feil", "Det oppsto en feil ved generering av kryptografisk nøkkelpar");
                            return;
                        }
                        publicKey = Utils.QueryKey(SecKeyType.EC, Settings.PUBLIC_KEY_APPLICATION_TAG);
                    }

                    var success = await SubmitQRCodeResultsAsync(hash, baseUrl, publicKey);

                    if(success)
                    {
                        // save base url and firebase token to database or json file
                    }
                    else
                    {
                        Utils.RemoveKeyPair(SecKeyType.EC, Settings.PRIVATE_KEY_APPLICATION_TAG, Settings.PUBLIC_KEY_APPLICATION_TAG);
                    }
                }
            });
        }

        public async Task<bool> SubmitQRCodeResultsAsync(string hash, string baseUrl, SecKey publicKey)
        {
            HttpResponseMessage result = default(HttpResponseMessage);

            try
            {
                using (var client = new HttpClient())
                {
                    var content = new FormUrlEncodedContent(new[] {
                        new KeyValuePair<string, string>("hash", hash),
                        new KeyValuePair<string, string>("deviceId", UIDevice.CurrentDevice.IdentifierForVendor.ToString()),
                        new KeyValuePair<string, string>("deviceName", UIDevice.CurrentDevice.Name),
                        new KeyValuePair<string, string>("deviceType", ((int)DeviceType.iOS).ToString(CultureInfo.InvariantCulture) ),
                        new KeyValuePair<string, string>("devicePushId", Messaging.SharedInstance.FcmToken),
                        new KeyValuePair<string, string>("devicePublicKey", Convert.ToBase64String(publicKey.ConvertToDER())),
                        new KeyValuePair<string, string>("devicePublicKeyFormat", "X.509"),
                        new KeyValuePair<string, string>("devicePublicKeyAlgorithm", "EC")
                    });

                    result = await client.PostAsync($"{baseUrl}Api/EnableTwoFactorAuthentication", content);

                    result.EnsureSuccessStatusCode();
                }
            }
            catch (HttpRequestException)
            {
                if (result.StatusCode == HttpStatusCode.Unauthorized)
                {
                    PresentAlert("Feil", "Ugyldig brukernavn eller passord");
                }
                else if (result.StatusCode == HttpStatusCode.Forbidden)
                {
                    PresentAlert("Feil", "Det fins allerede en enhet knyttet til din bruker");
                }

                return false;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            return true;
        }

        public void PresentAlert(string title, string message, IEnumerable<UIAlertAction> actions = null)
        {
            var alertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);

            if (actions == null)
            {
                alertController.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Default, null));
            }
            else
            {
                foreach (var a in actions)
                {
                    alertController.AddAction(a);
                }
            }

            PresentViewController(alertController, true, null);
        }
    }

    class MetaDataObjectDelegate : AVCaptureMetadataOutputObjectsDelegate
    {
        public Action<AVCaptureMetadataOutput, AVMetadataObject[], AVCaptureConnection> DidOutputMetadataObjectsAction;

        public override void DidOutputMetadataObjects(AVCaptureMetadataOutput captureOutput, AVMetadataObject[] metadataObjects, AVCaptureConnection connection)
        {
            if (DidOutputMetadataObjectsAction != null)
                DidOutputMetadataObjectsAction.Invoke(captureOutput, metadataObjects, connection);
        }
    }
}