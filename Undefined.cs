﻿using System;
namespace Alumis.Whistleblower.Authenticator.iOS
{
    sealed class Undefined
    {
        internal static readonly Undefined Singleton = new Undefined();

        public override string ToString()
        {
            return "<undefined>";
        }
    }
}
