﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using Security;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public static class Api
    {
        public static async Task EnableTwoFactorAuthenticationAsync(string baseUrl, string hash, string deviceId, string deviceName, DeviceType deviceType, string devicePushId, string devicePublicKey, string devicePublicKeyFormat, string devicePublicKeyAlgorithm)
        {
            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(new[] {
                    new KeyValuePair<string, string>("hash", hash),
                    new KeyValuePair<string, string>("deviceId", deviceId),
                    new KeyValuePair<string, string>("deviceName", deviceName),
                    new KeyValuePair<string, string>("deviceType", ((int)deviceType).ToString(CultureInfo.InvariantCulture) ),
                    new KeyValuePair<string, string>("devicePushId", devicePushId),
                    new KeyValuePair<string, string>("devicePublicKey", devicePublicKey),
                    new KeyValuePair<string, string>("devicePublicKeyFormat", devicePublicKeyFormat),
                    new KeyValuePair<string, string>("devicePublicKeyAlgorithm", devicePublicKeyAlgorithm)
                });

                var result = await client.PostAsync($"{baseUrl}Admin/EnableTwoFactorAuthentication", content);

                result.EnsureSuccessStatusCode();
            }
        }

        public static async Task RespondToChallengeAsync(byte[] challenge, byte[] signature, string baseUrl, string pushId)
        {
            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(new[] {
                    new KeyValuePair<string, string>("firebaseToken", pushId),
                    new KeyValuePair<string, string>("challenge", Convert.ToBase64String(challenge)),
                    new KeyValuePair<string,string>("signature", Convert.ToBase64String(signature)) 
                });

                var request = await client.PostAsync($"{baseUrl}Admin/RespondToChallenge", content);

                request.EnsureSuccessStatusCode();
            }
        }
    }
}
