﻿using System;
using System.Collections.Generic;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public class ComputedObservable<T> : Observable<T>, IDisposable
    {
        internal ComputedObservable(Func<ComputedObservable<T>, T> func) : base()
        {
            _func = func;
            ValueField = func(this);
        }

        Func<ComputedObservable<T>, T> _func;

        Dictionary<IObservable, ObservableSubscription> _observables = new Dictionary<IObservable, ObservableSubscription>();

        public override T Value
        {
            set
            {
                throw new NotSupportedException();
            }
        }

        internal TValue GetValue<TValue>(IObservableValue<TValue> observable)
        {
            var value = observable.Value;

            if (!_observables.ContainsKey(observable))
                _observables[observable] = observable.Subscribe(Refresh);

            return value;
        }

        internal ObservableSet<T> GetValue<T>(ObservableSet<T> set)
            where T : class
        {
            if (!_observables.ContainsKey(set))
                _observables[set] = set.Subscribe(Refresh);

            return set;
        }

        public void Refresh()
        {
            foreach (var p in _observables)
                p.Value.Dispose();

            _observables.Clear();
            base.Value = _func(this);
        }

#if DEBUG

        bool _disposed;

#endif

        public void Dispose()
        {

#if DEBUG
            if (_disposed)
                throw new InvalidProgramException();

            _disposed = true;
#endif

            foreach (var p in _observables)
                p.Value.Dispose();
        }
    }
}
