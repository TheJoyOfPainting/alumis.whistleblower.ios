﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public class ObservableDictionary<TKey, TValue> : Dictionary<TKey, TValue>
        where TValue : class
    {
        internal ObservableDictionary()
        {
            (_head._next = _tail)._previous = _head;
        }

        ObservableSubscription _head = new ObservableSubscription(), _tail = new ObservableSubscription();

        internal ObservableSubscription Subscribe(Action<TKey, TValue, TValue> action)
        {
            return new ObservableSubscription(_head, action);
        }

        internal void NotifySet(TKey key, TValue value)
        {
            if (TryGetValue(key, out TValue removedValue))
            {
                this[key] = value;
                NotifySubscribers(key, value, removedValue);
                return;
            }

            this[key] = value;
            NotifySubscribers(key, value, null);
        }

        internal void NotifyRemove(TKey key)
        {
            if (Remove(key, out TValue removedValue))
                NotifySubscribers(key, null, removedValue);
        }

        internal void NotifyClear()
        {
            foreach (var k in Keys.ToList())
            {
                Remove(k, out TValue removedValue);
                NotifySubscribers(k, null, removedValue);
            }
        }

        protected void NotifySubscribers(TKey key, TValue newValue, TValue removedValue)
        {
            for (var node = _head._next; node != _tail; node = node._next)
            {
                var action = node._action as Action<TKey, TValue, TValue>;

                if (action != null)
                    action(key, newValue, removedValue);

                else ((Action)node._action)();
            }
        }
    }
}
