﻿using System;
using System.Diagnostics;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public class ObservableSubscription : IDisposable
    {
        internal ObservableSubscription()
        {

        }

        internal ObservableSubscription(ObservableSubscription head, object action)
        {
            (_previous = head)._next = (_next = head._next)._previous = this;
            _action = action;
        }

        internal object _action;

        internal ObservableSubscription _previous, _next;

#if DEBUG

        bool _isDisposed;
#endif

        public void Dispose()
        {

#if DEBUG
            if (_isDisposed)
            {
                Debugger.Break(); // You are disposing a subscription twice. It is not acceptable!
                throw new Exception();
            }

            _isDisposed = true;
#endif

            (_previous._next = _next)._previous = _previous;
        }
    }

    //class ObservableSubscription<T> : ObservableSubscription
    //{
    //    internal ObservableSubscription()
    //    {

    //    }

    //    internal ObservableSubscription(ObservableSubscription head, Action<T, T> action) : base(head, action)
    //    {
    //        _action = action;
    //    }
    //}
}
