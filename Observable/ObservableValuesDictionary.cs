﻿using System.Collections.Generic;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public class ObservableValuesDictionary<TKey> : Dictionary<TKey, Observable<object>>
    {
        internal Observable<object> Get(TKey key)
        {
            if (TryGetValue(key, out Observable<object> observable))
                return observable;

            return this[key] = new Observable<object>(Undefined.Singleton);
        }

        internal void Set(TKey key, object value)
        {
            if (TryGetValue(key, out Observable<object> observable))
                observable.Value = value;

            else this[key] = new Observable<object>(value);
        }
    }
}