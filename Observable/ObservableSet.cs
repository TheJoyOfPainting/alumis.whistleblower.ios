﻿using System;
using System.Collections.Generic;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public class ObservableSet<T> : HashSet<T>, IObservable
        where T : class
    {
        internal ObservableSet(IEnumerable<T> collection) : base(collection)
        {
            (_head._next = _tail)._previous = _head;
        }

        ObservableSubscription _head = new ObservableSubscription(), _tail = new ObservableSubscription();

        public ObservableSubscription Subscribe(Action<T, T> action)
        {
            return new ObservableSubscription(_head, action);
        }

        public ObservableSubscription Subscribe(Action action)
        {
            return new ObservableSubscription(_head, action);
        }

        internal void NotifyAdd(T item)
        {
            if (Add(item))
                NotifySubscribers(item, null);
        }

        internal void NotifyRemove(T item)
        {
            if (Remove(item))
                NotifySubscribers(null, item);
        }

        protected void NotifySubscribers(T addedItem, T removedItem)
        {
            for (var node = _head._next; node != _tail; node = node._next)
            {
                var action = node._action as Action<T, T>;

                if (action != null)
                    action(addedItem, removedItem);

                else ((Action)node._action)();
            }
        }
    }
}
