﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    [DebuggerDisplay("{ToString()}")]
    public class Observable<T> : IObservableValue<T>
    {
        protected Observable()
        {
            (_head._next = _tail)._previous = _head;
        }

        internal Observable(T value) : this()
        {
            ValueField = value;
        }

        ObservableSubscription _head = new ObservableSubscription(), _tail = new ObservableSubscription();

        public ObservableSubscription Subscribe(Action action)
        {
            return new ObservableSubscription(_head, action);
        }

        public virtual T Value
        {
            get
            {
                return ValueField;
            }

            set
            {
                if (!EqualityComparer<T>.Default.Equals(value, ValueField))
                {
                    var oldValue = ValueField;

                    ValueField = value;
                    NotifySubscribers(oldValue, value);
                }
            }
        }

        protected T ValueField;

        public virtual void Set(object value)
        {
            Value = (T)value;
        }

        public ObservableSubscription Subscribe(Action<T, T> action)
        {
            return new ObservableSubscription(_head, action);
        }

        protected void NotifySubscribers(T oldValue, T newValue)
        {
            for (var node = _head._next; node != _tail; node = node._next)
            {
                var action = node._action as Action<T, T>;

                if (action != null)
                    action(oldValue, newValue);

                else ((Action)node._action)();
            }
        }

        public void NotifySubscribers()
        {
            NotifySubscribers(ValueField, ValueField);
        }

        public override string ToString()
        {
            return ValueField?.ToString();
        }
    }
}
