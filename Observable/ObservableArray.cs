﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public class ObservableArray<T> : List<T>
    {
        internal ObservableArray()
        {
            (_head._next = _tail)._previous = _head;
        }

        internal ObservableArray(IEnumerable<T> collection) : base(collection)
        {
            (_head._next = _tail)._previous = _head;
        }

        ObservableSubscription _head = new ObservableSubscription(), _tail = new ObservableSubscription();

        public ObservableSubscription Subscribe(Action action)
        {
            return new ObservableSubscription(_head, action);
        }

        public ObservableSubscription Subscribe(Action<int, T[], T[]> action)
        {
            return new ObservableSubscription(_head, action);
        }

        protected void NotifySubscribers(int index, T[] addedItems, T[] removedItems)
        {
            for (var node = _head._next; node != _tail; node = node._next)
            {
                var action = node._action as Action<int, T[], T[]>;

                if (action != null)
                    action(index, addedItems, removedItems);

                else ((Action)node._action)();
            }
        }

        public void NotifyInsert(int index, T item)
        {
            Insert(index, item);
            NotifySubscribers(index, new[] { item }, new T[0]);
        }

        public void NotifyInsertRange(int index, IEnumerable<T> collection)
        {
            InsertRange(index, collection);
            NotifySubscribers(index, collection.ToArray(), new T[0]);
        }

        public void NotifyRemoveAt(int index)
        {
            var item = this[index];

            RemoveAt(index);
            NotifySubscribers(index, new T[0], new[] { item });
        }

        public void NotifyAdd(T item)
        {
            var index = Count;

            Add(item);
            NotifySubscribers(index, new[] { item }, new T[0]);
        }

        public void NotifyClear()
        {
            var removedItems = ToArray();

            Clear();
            NotifySubscribers(0, new T[0], removedItems);
        }

        public bool NotifyRemove(T item)
        {
            var index = IndexOf(item);

            if (index == -1)
                return false;

            RemoveAt(index);
            NotifySubscribers(index, new T[0], new[] { item });

            return true;
        }
    }
}
