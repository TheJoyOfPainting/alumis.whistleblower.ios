﻿using System;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public interface IObservable
    {
        ObservableSubscription Subscribe(Action action);
    }

    public interface IObservableValue : IObservable
    {
        void Set(object value);
    }

    public interface IObservableValue<T> : IObservableValue
    {
        T Value { get; set; }
    }
}
