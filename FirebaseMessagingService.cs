﻿using System;
using Firebase.CloudMessaging;
using Firebase.Core;
using Foundation;
using Security;

namespace Alumis.Whistleblower.Authenticator.iOS
{
    public class FirebaseMessagingService: NSObject, IMessagingDelegate
    {
        public Observable<string> FcmToken = new Observable<string>(null);

        public delegate void FcmTokenEventHanlder(string fcmToken);
        public event FcmTokenEventHanlder FcmTokenEvent;

        public delegate void RemoteMessageEventHandler(NSDictionary appData);
        public event RemoteMessageEventHandler RemoteMessagingEvent;

        public void Configure()
        {
            App.Configure();

            Messaging.SharedInstance.Delegate = this;
            Messaging.SharedInstance.ShouldEstablishDirectChannel = true;
        }

        [Export("messaging:didReceiveRegistrationToken:")]
        public void DidReceiveRegistrationToken(Messaging messaging, string fcmToken)
        {
            Console.WriteLine($"Firebase registration token: {fcmToken}");

            FcmTokenEvent?.Invoke(fcmToken);

            FcmToken.Set(fcmToken);
        }

        [Export("messaging:didReceiveMessage:")] 
        public void DidReceiveMessage(Messaging messaging, RemoteMessage remoteMessage)
        {
            Console.WriteLine("Service-DidReceiveMessage");
            Console.WriteLine(remoteMessage.AppData);

            RemoteMessagingEvent?.Invoke(remoteMessage.AppData);
        } 
    }
}
